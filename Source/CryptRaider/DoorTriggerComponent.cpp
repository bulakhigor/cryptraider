// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorTriggerComponent.h"

UDoorTriggerComponent::UDoorTriggerComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UDoorTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UDoorTriggerComponent::CheckActor()
{
    TArray<AActor*> Actors;
    GetOverlappingActors(Actors);
    for (AActor* Actor : Actors)
    {
        bool HasTag = Actor->ActorHasTag(TagName);
        
        if (HasTag)
        {
            return true;
        }
    }
    return false;
}

void UDoorTriggerComponent::BeginPlay()
{
    Super::BeginPlay();
}
