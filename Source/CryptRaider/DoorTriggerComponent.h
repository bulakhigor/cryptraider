// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "DoorTriggerComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CRYPTRAIDER_API UDoorTriggerComponent : public UBoxComponent
{
	GENERATED_BODY()

public:
    UDoorTriggerComponent();

    virtual void TickComponent(
        float DeltaTime, ELevelTick TickType,
        FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable)
    bool CheckActor();

protected:
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere)
    FName TagName;
    
};
