// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerComponent.h"
#include "Mover.h"

UTriggerComponent::UTriggerComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    AActor* AcceptableActor = GetAcceptableActor();
    if (!AcceptableActor)
    {
        Mover->SetShouldMove(false);
    }
    else
    {
        UPrimitiveComponent* Component = Cast<UPrimitiveComponent>(AcceptableActor->GetRootComponent());
        if (Component)
        {
            Component->SetSimulatePhysics(false);
        }
        AcceptableActor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
        AcceptableActor->SetActorRotation(FRotator(0.0f, 180.0f, 0.0f));
        // AcceptableActor->SetActorLocation(GetComponentLocation());
        Mover->SetShouldMove(true);
    }
}

void UTriggerComponent::SetMover(UMover* NewMover)
{
    Mover = NewMover;
}

void UTriggerComponent::BeginPlay()
{
    Super::BeginPlay();
}

AActor* UTriggerComponent::GetAcceptableActor() const
{
    TArray<AActor*> Actors;
    GetOverlappingActors(Actors);
    for (AActor* Actor : Actors)
    {
        bool HasTag = Actor->ActorHasTag(TagName);
        bool IsGrabbed = Actor->ActorHasTag("Grabbed");
        if (HasTag && !IsGrabbed)
        {
            return Actor;
        }
    }

    return nullptr;
}
