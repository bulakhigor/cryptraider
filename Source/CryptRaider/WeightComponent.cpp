// Fill out your copyright notice in the Description page of Project Settings.


#include "WeightComponent.h"

#include "Mover.h"
#include "SWarningOrErrorBox.h"
#include "Components/SphereComponent.h"

UWeightComponent::UWeightComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UWeightComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    AActor* AcceptableActor = GetAcceptableActor();
    if (!AcceptableActor)
    {
        Mover->SetShouldMove(false);
    }
    else
    {
        UPrimitiveComponent* Component = Cast<UPrimitiveComponent>(AcceptableActor->GetRootComponent());
        if (Component)
        {
            CurrentMass = Component->GetMass();
            // Component->SetSimulatePhysics(false);
        }
        if (CurrentMass >= TargetWeight)
        {
            AcceptableActor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
            // AcceptableActor->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
            // AcceptableActor->SetActorLocation(GetComponentLocation());
            Mover->SetShouldMove(true);
        }
    }
}

void UWeightComponent::SetMover(UMover* NewMover)
{
    Mover = NewMover;
}


void UWeightComponent::BeginPlay()
{
    Super::BeginPlay();
}

AActor* UWeightComponent::GetAcceptableActor() const
{
    TArray<AActor*> Actors;
    GetOverlappingActors(Actors);
    for (AActor* Actor : Actors)
    {
        bool IsGrabbed = Actor->ActorHasTag("Grabbed");
        if (!IsGrabbed)
        {
            return Actor;
        }
    }
    return nullptr;
}
