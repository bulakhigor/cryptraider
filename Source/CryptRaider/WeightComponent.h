// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "WeightComponent.generated.h"

class UMover;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CRYPTRAIDER_API UWeightComponent : public UBoxComponent
{
	GENERATED_BODY()

public:
    UWeightComponent();

    virtual void TickComponent(
        float DeltaTime, ELevelTick TickType,
        FActorComponentTickFunction* ThisTickFunction) override;

    UFUNCTION(BlueprintCallable)
    void SetMover(UMover* NewMover);

protected:
    virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere)
    float TargetWeight = 1.0f;

    float CurrentMass = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool IsOpening = false;

    UMover* Mover;

    AActor* GetAcceptableActor() const;
};
